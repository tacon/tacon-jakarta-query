package dev.tacon.jakartaee.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IRoot;
import dev.tacon.jakartaee.query.criteria.ISelection;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaQuery;

class SingleQuery<F, R> extends Query<ISingleQuery<F, R>, F, R> implements ISingleQuery<F, R> {

	private Class<R> resultClass;
	private ISelection<R> selection;

	SingleQuery(final EntityManager entityManager) {
		super(entityManager);
	}

	protected SingleQuery(final Query<?, F, ?> query, final Class<R> resultClass, final ISelection<R> selection) {
		super(query);
		this.resultClass = resultClass;
		this.selection = selection;
	}

	@Override
	protected Class<R> getResultClass() {
		return this.resultClass;
	}

	@Override
	protected void prepareSelect(final CriteriaQuery<R> criteriaQuery) {
		if (this.selection != null) {
			criteriaQuery.select(this.selection.resolve(this));
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> ISingleQuery<T, R> from(final IRoot<T> root, final IFrom<?, ?>... froms) {
		final SingleQuery<T, R> query = (SingleQuery<T, R>) this;
		query.fromInternal(root, froms);
		return query;
	}

	@Override
	public List<R> fetch() {
		return this.prepareQuery().getResultList();
	}

	@Override
	public Stream<R> stream() {
		return this.prepareQuery().getResultStream();
	}

	@Override
	public Optional<R> fetchFirst() {
		final TypedQuery<R> query = this.prepareQuery();
		query.setMaxResults(1);
		return query.getResultStream().findFirst();
	}

	@Override
	public R fetchSingleResult() {
		return this.prepareQuery().getSingleResult();
	}
}
