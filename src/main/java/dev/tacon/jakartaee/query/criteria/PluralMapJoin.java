package dev.tacon.jakartaee.query.criteria;

import dev.tacon.annotations.Nullable;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.MapJoin;
import jakarta.persistence.metamodel.MapAttribute;

public class PluralMapJoin<Z, K, V> extends BaseJoin<Z, V, MapAttribute<? super Z, K, V>, MapJoin<Z, K, V>> implements IMapJoin<Z, K, V> {

	PluralMapJoin(final IFrom<?, Z> parent, final MapAttribute<? super Z, K, V> attribute, final JoinType joinType) {
		super(parent, attribute, joinType);
	}

	PluralMapJoin(final IFrom<?, Z> parent, final MapAttribute<? super Z, K, V> attribute, final JoinType joinType, final @Nullable String identifier) {
		super(parent, attribute, joinType, identifier);
	}

	@Override
	protected MapJoin<Z, K, V> create(final From<?, Z> from) {
		return from.join(this.attribute, this.joinType);
	}
}
