package dev.tacon.jakartaee.query.criteria;

import java.util.function.Function;

import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.metamodel.MapAttribute;
import jakarta.persistence.metamodel.PluralAttribute;
import jakarta.persistence.metamodel.SingularAttribute;

@FunctionalInterface
public interface IFrom<Z, X> extends IPath<X> {

	From<Z, X> create(final IQueryDataHolder data, AbstractQuery<?> criteriaQuery);

	default @Nullable String getIdentifier() {
		return null;
	}

	@Override
	default From<Z, X> resolve(final IQueryDataHolder data) {
		return data.getFrom(this);
	}

	default <Y> IJoin<X, Y> join(final SingularAttribute<? super X, Y> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <Y> IJoin<X, Y> join(final SingularAttribute<? super X, Y> attribute, final Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return this.join(attribute, JoinType.INNER).on(onCondition);
	}

	default <Y> IJoin<X, Y> join(final SingularAttribute<? super X, Y> attribute, final JoinType type) {
		return new SingleJoin<>(this, attribute, type);
	}

	default <Y> IJoin<X, Y> join(final SingularAttribute<? super X, Y> attribute, final JoinType type, final String identifier) {
		return new SingleJoin<>(this, attribute, type, identifier);
	}

	default <Y> IJoin<X, Y> join(final SingularAttribute<? super X, Y> attribute, final JoinType type, final Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return new SingleJoin<>(this, attribute, type).on(onCondition);
	}

	default <C, Y> IJoin<X, Y> join(final PluralAttribute<? super X, C, Y> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <C, Y> IJoin<X, Y> join(final PluralAttribute<? super X, C, Y> attribute, final Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return this.join(attribute, JoinType.INNER).on(onCondition);
	}

	default <C, Y> IJoin<X, Y> join(final PluralAttribute<? super X, C, Y> attribute, final JoinType type) {
		return new PluralJoin<>(this, attribute, type);
	}

	default <C, Y> IJoin<X, Y> join(final PluralAttribute<? super X, C, Y> attribute, final JoinType type, final String identifier) {
		return new PluralJoin<>(this, attribute, type, identifier);
	}

	default <C, Y> IJoin<X, Y> join(final PluralAttribute<? super X, C, Y> attribute, final JoinType type, final Function<IJoin<X, Y>, IExpression<Boolean>> onCondition) {
		return new PluralJoin<>(this, attribute, type).on(onCondition);
	}

	default <K, V> IMapJoin<X, K, V> join(final MapAttribute<? super X, K, V> attribute) {
		return this.join(attribute, JoinType.INNER);
	}

	default <K, V> IMapJoin<X, K, V> join(final MapAttribute<? super X, K, V> attribute, final JoinType type) {
		return new PluralMapJoin<>(this, attribute, type);
	}

	default <K, V> IMapJoin<X, K, V> join(final MapAttribute<? super X, K, V> attribute, final JoinType type, final String identifier) {
		return new PluralMapJoin<>(this, attribute, type, identifier);
	}
}
