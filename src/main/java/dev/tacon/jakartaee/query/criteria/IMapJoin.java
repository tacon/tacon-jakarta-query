package dev.tacon.jakartaee.query.criteria;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.MapJoin;

public interface IMapJoin<Z, K, V> extends IPluralJoin<Z, V> {

	@Override
	MapJoin<Z, K, V> resolve(IQueryDataHolder data);

	default IPath<K> key() {
		return data -> this.resolve(data).key();
	}

	default IPath<V> value() {
		return data -> this.resolve(data).value();
	}

	default IExpression<Map.Entry<K, V>> entry() {
		return data -> this.resolve(data).entry();
	}

	@Override
	default IMapJoin<Z, K, V> on(final Function<IJoin<Z, V>, IExpression<Boolean>> condition) {
		return data -> this.resolve(data).on(condition.apply(this).resolve(data));
	}

	default IMapJoin<Z, K, V> on(final BiFunction<IPath<K>, IPath<V>, IExpression<Boolean>> condition) {
		return data -> this.resolve(data).on(condition.apply(this.key(), this.value()).resolve(data));
	}
}
