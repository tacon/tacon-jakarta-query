package dev.tacon.jakartaee.query.criteria;

import java.util.Objects;

import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.AbstractQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.metamodel.Attribute;

abstract class BaseJoin<S, T, A extends Attribute<? super S, ?>, J extends Join<S, T>> implements IJoin<S, T> {

	private final IFrom<?, S> parent;
	protected final A attribute;
	protected final JoinType joinType;
	protected final @Nullable String identifier;

	BaseJoin(final IFrom<?, S> parent, final A attribute, final JoinType joinType) {
		this(parent, attribute, joinType, null);
	}

	BaseJoin(final IFrom<?, S> parent, final A attribute, final JoinType joinType, final @Nullable String identifier) {
		this.parent = parent;
		this.attribute = attribute;
		this.joinType = joinType;
		this.identifier = identifier;
	}

	@Override
	public @Nullable String getIdentifier() {
		return this.identifier;
	}

	@Override
	public J create(final IQueryDataHolder data, final AbstractQuery<?> criteriaQuery) {
		return this.create(this.parent.resolve(data));
	}

	protected abstract J create(From<?, S> from);

	@Override
	public J resolve(final IQueryDataHolder data) {
		@SuppressWarnings("unchecked")
		final J foundJoin = (J) data.getFrom(this);
		if (foundJoin != null) {
			final Attribute<? super S, ?> foundJoinAttribute = foundJoin.getAttribute();
			if (this.attribute.getDeclaringType().getJavaType() != foundJoinAttribute.getDeclaringType().getJavaType()) {
				throw new IllegalStateException("Source class " + this.attribute.getJavaType() + " does not match " + foundJoinAttribute.getJavaType() + " for join with identifier `" + this.identifier + "`");
			}
			if (!this.attribute.getName().equals(foundJoinAttribute.getName())) {
				throw new IllegalStateException("Attribute `" + this.attribute.getName() + "` does not match `" + foundJoinAttribute.getName() + "` for join with identifier `" + this.identifier + "`");
			}
			if (this.joinType != foundJoin.getJoinType()) {
				throw new IllegalStateException("Inconsistent JoinType." + this.joinType + " for join with identifier `" + this.identifier + "`");
			}

			return foundJoin;
		}

		final From<?, S> from = Objects.requireNonNull(
				this.parent.resolve(data),
				() -> "Cannot resolve parent join for " + this.attribute);

		final J join = this.create(from);
		data.registerJoin(this, join);
		return join;
	}
}
