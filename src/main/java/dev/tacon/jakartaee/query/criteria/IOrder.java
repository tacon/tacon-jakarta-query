package dev.tacon.jakartaee.query.criteria;

import dev.tacon.jakartaee.query.IQueryDataHolder;
import jakarta.persistence.criteria.Order;

@FunctionalInterface
public interface IOrder {

	Order resolve(IQueryDataHolder data);
}
