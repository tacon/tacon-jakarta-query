package dev.tacon.jakartaee.query.criteria;

@FunctionalInterface
public interface IRoot<X> extends IFrom<X, X> {
	//
}
