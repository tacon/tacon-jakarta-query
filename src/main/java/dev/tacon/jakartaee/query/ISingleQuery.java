package dev.tacon.jakartaee.query;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import dev.tacon.jakartaee.query.criteria.IFrom;
import dev.tacon.jakartaee.query.criteria.IRoot;

public interface ISingleQuery<F, R> extends IQuery<ISingleQuery<F, R>, F> {

	<T> ISingleQuery<T, R> from(IRoot<T> from, IFrom<?, ?>... froms);

	default <T> ISingleQuery<T, R> from(final Class<T> fromClass) {
		return this.from(Expressions.from(fromClass));
	}

	/**
	 * Executes the query and gets the result.
	 *
	 * @return the query result.
	 * @see javax.persistence.Query#getResultList()
	 */
	List<R> fetch();

	/**
	 * Executes the query returning limited to one result
	 * and returns the result as {@code Optional}.
	 *
	 * @return an empty {@code Optional} if there are
	 *         no results or the first result is {@code null},
	 *         otherwise an {@code Optional} containing
	 *         the first result value.
	 */
	Optional<R> fetchFirst();

	/**
	 * @see javax.persistence.Query#getSingleResult()
	 */
	R fetchSingleResult();

	/**
	 * Executes the query and gets a {@code Stream}
	 * over the result.
	 * <p>
	 * This is equivalent to:
	 *
	 * <pre>
	 * fetch().stream()
	 * </pre>
	 *
	 * </p>
	 *
	 * @return the query result as {@code Stream}.
	 * @see javax.persistence.Query#getResultList()
	 */
	Stream<R> stream();
}
